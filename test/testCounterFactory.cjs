const counterFactory = require('../counterFactory.cjs');

counter1 = counterFactory();
counter2 = counterFactory();


counter1.increment();
console.log(counter2.increment());
counter1.decrement();
console.log(counter1.decrement());
counter2.increment();
console.log(counter2.decrement());
counter1.increment();
counter2.increment();
console.log(counter1.increment());
counter1.increment();
console.log(counter2.increment());
console.log(counter1.decrement());