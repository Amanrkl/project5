const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

cb = () => {console.log('Hello')};

result = limitFunctionCallCount(cb,5)
result()


result = limitFunctionCallCount(cb,-3)
console.log(result())

result = limitFunctionCallCount([],2)
console.log(result())


cb = (a,b) => {console.log(a,b)};

result = limitFunctionCallCount(cb,5)
result("hello","World")
