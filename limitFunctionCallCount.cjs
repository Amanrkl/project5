function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    
    function calledForNTimes(...args){

        if ( typeof cb != 'function' || !Number.isInteger(n) || n<=0 ){
            return null
        }
        
        for (let i=0 ; i<n; i++){
            cb(...args)
        }
    }


    return calledForNTimes

}

module.exports = limitFunctionCallCount